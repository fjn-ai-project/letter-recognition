import string
import os

from PIL import Image

import torch                                    # PyTorch (cpu version is enough) is required
import torchvision
import torchvision.transforms as transforms

import torch.nn as nn
import torch.nn.functional as F

from torchvision import datasets, models



class ResNet(nn.Module):
    
    # define model architecture
    
    def __init__(self, num_classes):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 64, 3, 1, 'same')
        self.bn1 = nn.BatchNorm2d(64)
        self.conv2 = nn.Conv2d(64, 64, 3, 1, 'same')
        self.bn2 = nn.BatchNorm2d(64)
        
        self.iconv1 = nn.Conv2d(1, 64, kernel_size=1, stride=1, bias=False)
        self.ibn1 = nn.BatchNorm2d(64) 
        
        self.conv3 = nn.Conv2d(64, 128, 3, 2, 1)
        self.bn3 = nn.BatchNorm2d(128)        
        self.conv4 = nn.Conv2d(128, 128, 3, 1, 'same')
        self.bn4 = nn.BatchNorm2d(128)
        
        self.iconv2 = nn.Conv2d(64, 128, kernel_size=1, stride=2, bias=False)
        self.ibn2 = nn.BatchNorm2d(128)        
        
        self.conv5 = nn.Conv2d(128, 256, 3, 2, 1)
        self.bn5 = nn.BatchNorm2d(256)
        self.conv6 = nn.Conv2d(256, 256, 3, 1, 'same')
        self.bn6 = nn.BatchNorm2d(256)
        
        self.iconv3 = nn.Conv2d(128, 256, kernel_size=1, stride=2, bias=False)
        self.ibn3 = nn.BatchNorm2d(256)
        
        self.conv7 = nn.Conv2d(256, 512, 3, 2, 1)
        self.bn7 = nn.BatchNorm2d(512)
        self.conv8 = nn.Conv2d(512, 512, 3, 1, 'same')
        self.bn8 = nn.BatchNorm2d(512)
        
        self.iconv4 = nn.Conv2d(256, 512, kernel_size=1, stride=2, bias=False)
        self.ibn4 = nn.BatchNorm2d(512)
        
        self.fc = nn.Linear(512, num_classes)

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        
        
    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        x = out + self.ibn1(self.iconv1(x))
        out = F.relu(self.bn3(self.conv3(x)))
        out = self.bn4(self.conv4(out))
        x = out + self.ibn2(self.iconv2(x))
        out = F.relu(self.bn5(self.conv5(x)))
        out = self.bn6(self.conv6(out))
        x = out + self.ibn3(self.iconv3(x))
        out = F.relu(self.bn7(self.conv7(x)))
        out = self.bn8(self.conv8(out))
        x = out + self.ibn4(self.iconv4(x))
        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        return x

    
    
modelpath=os.path.dirname(__file__)+'/resnet_model.pt'

characters = string.ascii_uppercase

model = ResNet(len(characters))              # load model architecture
if torch.cuda.is_available():
    device = 'cuda'
else:
    device = 'cpu'
model.load_state_dict(torch.load(modelpath, map_location=torch.device(device))) # load weights+biases
model.eval()                                 # siwtch to eval-mode: no dropout/batchnorm
transform = transforms.Compose([
#    transforms.Resize(28),                  # not necessary if img resized
    transforms.Grayscale(),                 # not necessary if img already in greyscale (one channel)
    transforms.ToTensor(),
    transforms.Normalize((0.5,),(0.5,))
])


def imageToPred(img_path, model=model, transform=transform, characters=characters, n_top=-1):
    
    # input: preprocessed 28px*28px greyscale image file path as input
    # example: imageToPred("/some/path/M.bmp", n_top=3)
    # see `imageDataToPred`
    img = Image.open(img_path)
    return imageDataToPred(img, model, transform, characters, n_top)


def imageDataToPred(img, model=model, transform=transform, characters=characters, n_top=-1):

    # input: preprocessed 28px*28px greyscale image as input
    # optional arg:
    #     n_top: only output top n_top predictions, -1 means all
    # returns: list of tuples containing (predicted character, probability) sorted by probability
    # example: imageDataToPred(img, n_top=3)
    with torch.no_grad():                                # deactivate gradients (saves computing time)
        tensor = transform(img).unsqueeze(0)             # transform img to datatype that can be fed into model
        pred = F.softmax(model(tensor).squeeze(), dim=0) # convert network output into probabilities (sum=1.0)
        pred = [(prob.item(), characters[label])
                for label, prob in enumerate(pred)]      # assign char to each prob
        pred.sort(reverse=True)                          # sort (highest prob first)
        pred = [(c, p) for p,c in pred]                  # invert order of tuple elements (-> char first)
        if n_top!=-1: pred = pred[:n_top]                # make selection according to n_top
    
    return pred
